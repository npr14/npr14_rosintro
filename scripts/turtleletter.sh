#!/usr/bin/bash

# rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[4.0 , 0.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
# rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[5.0 , 0.0 , 0.0]' '[0.0 , 0.0 , -4.5]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0 , 0.0 , 0.0]' '[0.0 , 0.0 , 1.56]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[4.0 , 0.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0 , 0.0 , 0.0]' '[0.0 , 0.0 , -2.5]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[4.5 , 0.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0 , 0.0 , 0.0]' '[0.0 , 0.0 , 2.5]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[4.0 , 0.0 , 0.0]' '[0.0 , 0.0 , 0.0]'