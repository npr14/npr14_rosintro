#!/usr/bin/bash

rosservice call /turtle1/set_pen 0 0 0 0 1
rosservice call /turtle1/teleport_absolute 3.0 4.0 1.57
rosservice call /turtle1/set_pen 255 0 0 4 0

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[4.0 , 0.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0 , 0.0 , 0.0]' '[0.0 , 0.0 , -2.5]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[4.65 , 0.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0 , 0.0 , 0.0]' '[0.0 , 0.0 , 2.5]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[4.0 , 0.0 , 0.0]' '[0.0 , 0.0 , 0.0]'

rosservice call /turtle1/set_pen 0 0 0 0 1
rosservice call /turtle1/teleport_relative -- 1.5 -1.57
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0 , 0.0 , 0.0]' '[0.0 , 0.0 , -1.57]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[1.0 , 0.0 , 0.0]' '[0.0 , 0.0 , 0]'

rosservice call /turtle1/set_pen 0 255 0 4 0
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.3 , 0.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rosservice call /turtle1/set_pen 0 0 0 0 1
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.5 , 0.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rosservice call /turtle1/set_pen 0 255 0 4 0
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[2.2 , 0.0 , 0.0]' '[0.0 , 0.0 , 0.0]'