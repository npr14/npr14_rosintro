#!/usr/bin/env python

# Based on tutorial code found at:
# https://ros-planning.github.io/moveit_tutorials/doc/
# move_group_python_interface/move_group_python_interface_tutorial.html

# All imports
from __future__ import print_function

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

from math import pi, tau, dist, fabs, cos

from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

def move_to_start(move_group):
    # Move end effector in joint space to start pos, print success and return boolean
    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.orientation.w = 1.0
    pose_goal.position.x = 0.4
    pose_goal.position.y = 0.1
    pose_goal.position.z = 0.7
    print(pose_goal.orientation.x)
    print(pose_goal.orientation.y)
    print(pose_goal.orientation.z)
    move_group.set_pose_target(pose_goal)

    # Attempt to move to pose, return bool for planning/execution success
    success = move_group.go(wait=True)
    if success: print("Moving to initial pos succeeded")
    else: print("Moving to initial pos failed")

    # Stopping to ensure no residual movement
    move_group.stop()

    rospy.sleep(1)

    # Clear targets after planning with poses
    move_group.clear_pose_targets()

    return success

def draw_letter(move_group, waypoints):
    # From waypoints array, plan/execute path for letter, return bool
    # for planning/execution success

    # Interpolate the Cartesian path at a resolution of 1 cm, so specify 0.01
    # as the eef_step
    # Disable the jump threshold by setting it to 0.0, ignoring the check for 
    # infeasible jumps in joint space
    (plan, fraction) = move_group.compute_cartesian_path(
        waypoints, 0.01, 0.0)  # waypoints to follow  # eef_step  # jump_threshold 

    # Publish trajectory plan to view in RViz
    display_trajectory = moveit_msgs.msg.DisplayTrajectory()
    display_trajectory.trajectory_start = robot.get_current_state()
    display_trajectory.trajectory.append(plan)
    # Publish
    display_trajectory_publisher.publish(display_trajectory)

    # Execute the plan, following defined waypoint path
    success = move_group.execute(plan, wait=True)
    if success: print("Letter succeeded")
    else: print("Letter failed")

    # Stopping to ensure no residual movement
    move_group.stop()
    return success

# Initialize moveit_commander and a rospy node
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("first_initial", anonymous=True)

# Create RobotCommander object (provides info like kinematic model and
# current joint states)
robot = moveit_commander.RobotCommander()
# Create PlanningSceneInterface (remote interface for robot's internal
# understanding of surroundings)
scene = moveit_commander.PlanningSceneInterface()

# Create MoveGroupCommander, which interfaces to a planning group (in
# this case manipulator)
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)
# move_group.set_num_planning_attempts(50)


# Create publisher to display trajectories in RViz
display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

# Get the name of the planning reference frame
planning_frame = move_group.get_planning_frame()
print("============ Planning frame: %s" % planning_frame)

# Get move_group's end effector link name
eef_link = move_group.get_end_effector_link()
print("============ End effector link: %s" % eef_link)

# List all robot groups
group_names = robot.get_group_names()
print("============ Available Planning Groups:", robot.get_group_names())

# Print robot state (for debugging purposes)
print("============ Printing robot state")
print(robot.get_current_state())
print("")

move_to_start(move_group)

# Create waypoint list to move EE in the shape of the letter N
N_waypoints = []

wpose = move_group.get_current_pose().pose
wpose.position.z += 0.2  # move up (z)
N_waypoints.append(copy.deepcopy(wpose))

wpose.position.y += 0.12  # move diagonally down (z) and right (y)
wpose.position.z -= 0.2 
N_waypoints.append(copy.deepcopy(wpose))

wpose.position.z += 0.2 # move up (z)
N_waypoints.append(copy.deepcopy(wpose))

draw_letter(move_group, N_waypoints)

move_to_start(move_group)

# Create waypoint list to move EE in the shape of the letter P
P_waypoints = []

wpose = move_group.get_current_pose().pose
wpose.position.z += 0.2  # move up (z)
P_waypoints.append(copy.deepcopy(wpose))

wpose.position.y += 0.08  # move right (y)
P_waypoints.append(copy.deepcopy(wpose))

wpose.position.z -= 0.08 # move down (z)
P_waypoints.append(copy.deepcopy(wpose))

wpose.position.y -= 0.08  # move left (y)
P_waypoints.append(copy.deepcopy(wpose))

draw_letter(move_group, P_waypoints)

move_to_start(move_group)

# Create waypoint list to move EE in the shape of the letter R
R_waypoints = []

wpose = move_group.get_current_pose().pose
wpose.position.z += 0.2  # move up (z)
R_waypoints.append(copy.deepcopy(wpose))

wpose.position.y += 0.08  # move right (y)
R_waypoints.append(copy.deepcopy(wpose))

wpose.position.z -= 0.08 # move down (z)
R_waypoints.append(copy.deepcopy(wpose))

wpose.position.y -= 0.08  # move left (y)
R_waypoints.append(copy.deepcopy(wpose))

wpose.position.y += 0.08  # move diagonally down (z) and right (y)
wpose.position.z -= 0.12
R_waypoints.append(copy.deepcopy(wpose))

draw_letter(move_group, R_waypoints)

print("All letters finished")